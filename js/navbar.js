(function(){
	var app = angular.module('navbar', [])
	app.directive('navbarTop', function(){
		return {
			restrict: 'E',
			templateUrl: 'includes/navbar_top.html'
		}	
	});

})();