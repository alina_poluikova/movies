 (function(){
  var app = angular.module('seasons', [])
    app.controller('TabCtrl', function(){
    this.tab = 1;

    this.setTab = function(tab){
      this.tab = tab;
    };

    this.isSet = function(tab){
      return (this.tab === tab);
    };
  });
   app.directive("seasonSeries", function() {
      return {
        restrict: 'E',
        templateUrl: "includes/season-series.html"
      };
    });

    app.directive("seasonDescription", function() {
      return {
        restrict: 'E',
        templateUrl: "includes/season-description.html"
      };
    });

    app.directive("seasonComments", function() {
      return {
        restrict:"E",
        templateUrl: "includes/season-comments.html"
      };
    });


  app.controller('StoreController', ['$http', '$stateParams', function($http, $stateParams){
    var store = this;
    store.seasonId = $stateParams.seasonId;
    store.seasons = [];
    store.currentSeason = {};
    $http.get('/store-products.json').success(function(data){
      store.seasons = data;
      for (var i = store.seasons.length - 1; i >= 0; i--) {
        if (store.seasons[i].seasonId == $stateParams.seasonId){
          store.currentSeason = store.seasons[i];
          console.log($stateParams);
        }
      };
    });    
  }]);
 
  app.controller('ReviewController', function(){
    this.review = {};
    this.addReview = function(season){
      console.log(season)
      this.review.createdOn = Date.now();
      season.reviews.push(this.review);
      this.review = {};
    };
  });


})();

