(function(){
	var app = angular.module('mainApp', ['ui.router', 'zumba.angular-waypoints', 'navbar', 'angular-carousel', 'home', 'seasons', 'serias'])

	app.config(function($stateProvider, $urlRouterProvider, $sceProvider) {
    $urlRouterProvider.otherwise('/home')
    $stateProvider
    .state('home', {
      url: '/home',
      templateUrl: 'templates/home.html',
      controller: 'HomeCtrl'
    })
    .state('seasons', {
      url: '/seasons/:seasonId',
      templateUrl: 'templates/season.html',
      controller: 'StoreController',
      controllerAs: 'store',
    })
    .state('seria', {
      url: '/seasons/:seasonId/seria/:seriaId',
      templateUrl: 'templates/online-seria.html',
      controller: 'SeriaCtrl',
      controllerAs: 'ser'
    })
    .state('actors', {
      url: '/actors',
      templateUrl: 'templates/actors.html'
    })
    .state('music', {
      url: '/music',
      templateUrl: 'templates/musik.html'
    })
    .state('news', {
      url: '/news',
      templateUrl: 'templates/movies.html'
    })
     .state('movie2008', {
      url: '/movie_2008',
      templateUrl: 'templates/movie_2008.html'
    })
    .state('movie2010', {
      url: '/movie_2010',
      templateUrl: 'templates/movie_2010.html'
    });
       $sceProvider.enabled(false);

  });



})();