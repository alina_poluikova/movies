(function(){
  var app = angular.module('serias', [])
    app.controller('TabCtrlSeria', function(){
    this.tab = 1;

    this.setTab = function(tab){
      this.tab = tab;
    };

    this.isSet = function(tab){
      return (this.tab === tab);
    };
  });
   app.directive("seriaVideo", function() {
      return {
        restrict: 'E',
        templateUrl: "includes/seria-video.html"
      };
    });

    app.directive("seriaDescription", function() {
      return {
        restrict: 'E',
        templateUrl: "includes/seria-description.html"
      };
    });

    app.directive("seriaComments", function() {
      return {
        restrict:"E",
        templateUrl: "includes/seria-comments.html"
      };
    });


  app.controller('SeriaCtrl', ['$http', '$stateParams', function($http, $stateParams){
    var ser = this;
    ser.serias = [];
    ser.seasonId = $stateParams.seasonId;
    ser.currentSeria = {};
    $http.get('/store-products.json').success(function(seriasData){
      ser.serias = seriasData[parseInt(ser.seasonId)-1].serias;
      for (var i = ser.serias.length - 1; i >= 0; i--) {
        if (ser.serias[i].seriaId == $stateParams.seriaId) {
          ser.currentSeria = ser.serias[i];
          console.log(ser.currentSeria);

        }
      };
    });
  }]);


})();